<?php
function connect()
{
    global $connect;
    if (!is_a($connect, "PDO")) {
        try {
            $connect = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ';charset=utf8', DB_USER, DB_PASSWORD);
        } catch (PDOException $e) {
            die ('Erreur: ' . $e->getMessage());
        }
    }
    return $connect;
}