<?php
function getContent(string $page){
    if (is_array(EXTENSIONS)){
        foreach (EXTENSIONS as $ext){
            $nomdoc=$page.$ext;
            if (file_exists($nomdoc)){
                include $nomdoc;
            }
        }
    }
}

function getListCours(): false|array
{
    global $connect ;
    $request=$connect->prepare("select name, code from course");
    $request->execute();
    return $request->fetchAll(PDO::FETCH_ASSOC);

}
function getListUsers(): false|array
{
    global $connect ;
    $request=$connect->prepare("select * from user");
    $request->execute();
    return $request->fetchAll(PDO::FETCH_ASSOC);

}

function checkDouble(string $value,string $champ)
{
    $listuser = getListUsers();
    $list = [];
    foreach ($listuser as $user) {
        $list[] = $user[$champ];
    }
    if (in_array($value, $list)) {
        return false;
    } else {
        return true;
    }
}

function getUser(string $champ,string $value){
    global $connect;
    $param=[$value];
    $query=$connect ->prepare('select * from user where '.$champ.' = ? ');
    $query->execute($param);
    return $query->fetchObject();
}

function lastloginuser(int $id){
    global $connect;
    $param=[$id];
    $query=$connect->prepare('update user set lastlogin = now() where id = ?');
    $query->execute($param);
}

function exportjson(object $user): void{
    $filename = $user->id .'_'.$user->username. '.json';
    header('Content-type: application/json');
    header('Content-disposition: attachment; filename="' . $filename . '"');
    echo json_encode($user);
}