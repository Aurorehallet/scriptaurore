<?php

session_name('examscript'.date('y-m-d'));
session_start(['cookie_lifetime'=>3600]);

include_once '../config.php';
include_once '../library/function.php';
include_once '../library/pdo.php';
$connect=connect();

if (!empty($_SESSION['userid'])) {

    $user = getUser('id', $_SESSION['userid']);

    unset($user->password);
    unset($user->image);
    exportjson($user);
}