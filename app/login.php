<?php
if (!empty($_POST['username']) && (!empty($_POST['password']))) {
    if (!checkdouble($_POST['username'],'username')){
        $user=getUser('username',$_POST['username']);
        if (password_verify($_POST['password'], $user->password)){
            $_SESSION['userid']=$user->id;
            lastloginuser($user->id);
            $_SESSION['alert']='Bienvenue à '.$user->username;
            $_SESSION['alert-color']='success';
            header('location:index.php?page=page/profil');
            die;
        } else {
            $_SESSION['alert']='Echec de l\'authentification car le mot de passe est incorrect';
            header('location:index.php?page=page/login');
            die;
        }
    } else {
        $_SESSION['alert']='Echec de l\'authentification car le nom d\'utilisateur n\'existe pas';
        header('location:index.php?page=page/inscription');
        die;
    }
} else {
    $_SESSION['alert']='Veuillez remplir tous les champs';
    header('location:index.php?page=page/login');
    die;
}
