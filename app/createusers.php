<?php
if (!empty($_POST['username']) && (!empty($_POST['password'])) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    foreach ($_POST as $key => $value){
        $$key = $value;
    }
    if (checkDouble($username, 'username')){
        if (checkDouble($email, 'email')){
            if (!getListUsers()){
                $admin='admin, ';
                $adminvalue='1, ';
            } else {
                $admin='';
                $adminvalue='';
            }
           global $connect;
           $query=$connect ->prepare('insert into user(username, password, email, '.$admin.' created, lastlogin)
values (?,?,?, '.$adminvalue.' now(),now())');
           $param=[trim($username), password_hash($password, PASSWORD_DEFAULT), $email];
           $query->execute($param);
           if ($query->rowCount()){
               $userId=$connect->lastInsertId();
               echo 'Bienvenue à '.$username;
               if (!empty($_FILES)){
                   if ($_FILES['photo']['type']=='image/jpeg' or $_FILES['photo']['type']=='image/png' or $_FILES['photo']['type']=='image/jpg'){
                       if ($_FILES['photo']['size']<1024000){
                           $imgpath=ROOT_PATH.'\\img\\profilimg\\';
                           if (!is_dir($imgpath.$userId)){
                               mkdir($imgpath.$userId);
                           }
                           $baseName=$userId.'.'.substr($_FILES['photo']['type'], 6,4);
                           $move=move_uploaded_file($_FILES['photo']['tmp_name'], $imgpath.$userId.'\\'.$baseName);
                       }

                   }
               }
           } else {
               $_SESSION['alert']='échec de l\'authentification';
           }
        } else {
            $_SESSION['alert']='Cet email est déjà utilisé';
        }

    } else {
        echo 'Ce nom d\'utilisateur est déjà utilisé';
    }
}
