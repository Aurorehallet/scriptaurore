<?php
if (!empty($_SESSION['userid'])) {
    if ($_SESSION['userid'] == $_POST['id']) {
        $password=$_POST['password'];
        $email=$_POST['email'];
        $user=getUser('id', $_SESSION['userid']);
        if (!checkDouble($email, 'email') && empty($password)) {

        } else{
            $sql= 'update user set ';
            $param=[];
            $message='';
            if (!empty($password)){
                if (!password_verify($password, $user->password)){
                    $sql.='password = ?';
                    $param[]=password_hash($password, PASSWORD_DEFAULT);
                    $message.='Votre mot de passe a été mis à jour <br>';

                }
            }

            if (filter_var($email, FILTER_VALIDATE_EMAIL)){
                if (count($param)>0){
                    $sql.=', ';
                }
                $sql.='email = ? ';
                $param[]=$email;
                $message.='Votre email a été mis à jour';
            }
            if (!empty($param)){
                $sql.= 'where id = ? ';
                $param[]=$_POST['id'];
                $connect=connect();
                $query=$connect->prepare($sql);
                $query->execute($param);
                $_SESSION['alert']=$message;
                $_SESSION['alert-color']='success';
                header('location:index.php?page=page/profil');
                die;
            }
        }

    }

} else {
    $_SESSION['alert'] = 'Connecte toi!!!!!!!';
    header('location:index.php?page=page/inscription');
    die;
}
// update user set password = ?,email = ?