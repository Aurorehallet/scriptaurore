<?php
session_name('examscript'.date('y-m-d'));
session_start(['cookie_lifetime'=>3600]);
if (!empty($_SESSION['alert'])){
    if (!empty($_SESSION['alert-color'])&& in_array($_SESSION['alert-color'],['danger','info','success','warning'])){
    $alertColor=$_SESSION['alert-color'];
    unset($_SESSION['alert-color']);

    } else {
        $alertColor='danger';
    }
    echo '<div class="alert alert-'.$alertColor.'">'.$_SESSION['alert'].'</div>';
    unset($_SESSION['alert']);
}
require_once "config.php";
require_once "library/pdo.php";
require_once "library/function.php";
$connect=connect();
require_once "page/header.html";
require_once "page/nav.php";

if (!empty($_GET["page"])) {
    getContent($_GET["page"]);
}
require_once "page/footer.html";