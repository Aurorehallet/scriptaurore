<?php
?>
<nav class="navbar">
    <a class="nav-item" href="index.php">Accueil</a>
    <a class="nav-item" href="index.php?page=page/listedescours">Liste des cours</a>

    <?php
    if (!empty($_SESSION['userid'])){
        echo '<a class="nav-item" href="index.php?page=page/profil">Profil</a>
           <a class="nav-item" href="index.php?page=page/logout">Log out</a>';
        if (getUser('id',$_SESSION['userid'])->admin){
           echo '<a class="nav-item" href="index.php?page=page/admin">Admin</a>';
        }
    } else {
       echo '<a class="nav-item" href="index.php?page=page/login">Log in</a>';
    }
    ?>

</nav>
